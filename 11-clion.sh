#!/usr/bin/env bash

working_container=$(buildah from sircsalot/csl3-clone:11)

buildah run "${working_container}" sh -c 'ln -sft /usr/bin/ /opt/rh/devtoolset-11/root/usr/bin/*'
buildah run "${working_container}" ln -s /usr/bin/cmake{3,}

buildah commit "${working_container}" csl3-clone:11-clion
