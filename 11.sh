#!/usr/bin/env bash

working_container=$(buildah from centos:7)

buildah run "${working_container}" yum upgrade -y
buildah run "${working_container}" yum install -y dnf
buildah run "${working_container}" dnf install -y http://repo.okay.com.mx/centos/7/x86_64/release/okay-release-1-1.noarch.rpm
buildah run "${working_container}" dnf install -y centos-release-scl
buildah run "${working_container}" dnf install -y devtoolset-11
buildah run "${working_container}" sh -c 'echo "alias gcc-4.8.5='"'"'/usr/bin/gcc'"'"'" >> /etc/bashrc'
buildah run "${working_container}" sh -c 'echo "alias gcc-11='"'"'/opt/rh/devtoolset-11/root/usr/bin/gcc'"'"'" >> /etc/bashrc'
buildah run "${working_container}" sh -c 'echo "source /opt/rh/devtoolset-11/enable" >> /etc/bashrc'
buildah run "${working_container}" dnf install -y vim wget tree git
buildah run "${working_container}" dnf install -y cmake3 ninja-build
buildah run "${working_container}" sh -c 'echo "alias cmake='"'"'cmake3'"'"'" >> /etc/bashrc'
buildah run "${working_container}" dnf install -y python3
buildah run "${working_container}" dnf autoremove -y
buildah run "${working_container}" pip3 install -U ipython ipdb pdbpp pip

buildah commit "${working_container}" csl3-clone:11
