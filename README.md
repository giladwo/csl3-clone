# csl3-clone ([Docker Hub](https://hub.docker.com/r/sircsalot/csl3-clone))

Containers that emulate `csl3` (for mtm etc):
- `sircsalot/csl3-clone:4`: outdated, just like the original csl3
- `sircsalot/csl3-clone:11`: current, old `gcc` still available with `gcc-4.8.5`

Patched for CLion, so that all binaries are where they're expected:
- `sircsalot/csl3-clone:4-clion`: outdated
- `sircsalot/csl3-clone:11-clion`: current
<br/>
Both versions add a few extras not strictly required for mtm, i.e. `ipython`,
`wget` etc.
<br/>
`11` adds more quality of life improvements, i.e. `ninja-build` (more
performant out of the box then `make`)


These are intended to be beginner-friendly batteries-included alternatives for students.
<br/>
Compared to csl3, these are
- [x] Not shared with hundreds of other students.
- [x] Not utterly outdated (can use `11` and only test on `4` before submission).
- [x] Don't require vpn (or being on campus). (In fact, internet not required after initial download).
- [x] Not used over the internet, avoiding much unnecessary overhead.

## Why not just use wsl?
Go ahead, wsl is awesome!
<br/>
However, the closest official wsl distro to csl3 is [Fedora Remix](https://github.com/WhitewaterFoundry/Fedora-Remix-for-WSL) and may be too up to date for beginners (does not support gcc-4.8.5).
<br/>
The most popular wsl distro (Ubuntu) is both unrelated to rhel and sports gcc 9 as of now.
<br/>
Thus, for final `gcc-4.8.5` compatibility check `csl3` is still needed (or `sircsalot/csl3-clone:4` :wink:).


If any of the above bugs you, give these images a try!


### Docker installation
To install `docker` (or quivalent, i.e. `podman`) see [the official instructions](https://docs.docker.com/desktop/windows/install/).
In a nutshell:
- Windows: `winget install docker.dockerdesktop` (or [download docker desktop from the official website](https://hub.docker.com/editions/community/docker-ce-desktop-windows))
- Debian-based (i.e. Ubuntu): `sudo apt update && sudo apt install docker.io`
- Fedora: `sudo dnf install podman`
- Centos / RHEL: `sudo yum install podman`


## Examples
```bash
$ # recommended, updated toolchain (old gcc available as `gcc-4.8.5`) + extras
$ docker run sircsalot/csl3-clone:11

$ # original (fewer extras, old toolchain)
$ docker run sircsalot/csl3-clone:4
```


### CLion etc
Follow [the official instructions](https://www.jetbrains.com/help/clion/clion-toolchains-in-docker.html#create-docker-toolchain) and select `sircsalot/csl3-clone:11` (or `sircsalot/csl3-clone:4`) on step 3 as image.
