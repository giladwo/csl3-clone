#!/usr/bin/env bash

working_container=$(buildah from centos:7)

buildah run "${working_container}" yum upgrade -y
buildah run "${working_container}" yum install -y gcc gcc-c++
buildah run "${working_container}" yum install -y valgrind gdb strace
buildah run "${working_container}" yum install -y http://repo.okay.com.mx/centos/7/x86_64/release/okay-release-1-1.noarch.rpm
buildah run "${working_container}" yum install -y make cmake3
buildah run "${working_container}" sh -c 'echo "alias cmake='"'"'cmake3'"'"'" >> /etc/bashrc'
buildah run "${working_container}" yum install -y python3 which tree vim git openssh-clients
buildah run "${working_container}" yum autoremove -y
buildah run "${working_container}" pip3 install -U ipython ipdb pdbpp pip

buildah commit "${working_container}" csl3-clone:4
