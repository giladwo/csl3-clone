#!/usr/bin/env bash

working_container=$(buildah from sircsalot/csl3-clone:4)

buildah run "${working_container}" ln -s /usr/bin/cmake{3,}

buildah commit "${working_container}" csl3-clone:4-clion
